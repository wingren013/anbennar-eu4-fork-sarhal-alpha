owner = L81
controller = L81
add_core = L81
culture = mashkay_lizardfolk
religion = anzalkatsa

hre = no

base_tax = 6
base_production = 7
base_manpower = 6

trade_goods = dyes

capital = ""

is_city = yes
fort_15th = yes
# No previous file for Anbennar5951

center_of_trade = 1