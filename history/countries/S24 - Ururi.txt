setup_vision = yes
government = native
add_government_reform = native_chiefdom_reform
government_rank = 1
primary_culture = guryadagga
religion = ilaakhidigaha_yaghin
technology_group = tech_cannorian

capital = 6080

1000.1.1 = { set_estate_privilege = estate_mages_organization_state }